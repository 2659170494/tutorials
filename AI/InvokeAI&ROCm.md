# Invoke AI with 7800XT
## Installation
Create a container for the install. I'm using distrobox here.
```
distrobox create ubuntu -i ubuntu:latest
```

Get the InvokeAI Installer.
https://github.com/invoke-ai/InvokeAI/releases/

Enter the container and install InvokeAI.
```
distrobox enter ubuntu
sudo apt install python3.10-venv rocm*
cd InvokeAI-Installer
./install.sh
```
## Run InvokeAI
Run the correct HSA_GFX_VERSION and test it. The following is for 7800XT. I believe you can use 10.3.0 for 6000 series cards.
```
cd [your invokeai dir]
HSA_OVERRIDE_GFX_VERSION=11.0.0 ./invoke.sh
```

If all works then just add the variable to the invoke.sh script. I added mine above the 'set -eu'
```
vim invoke.sh
add export HSA_OVERRIDE_GFX_VERSION=11.0.0
```

## 