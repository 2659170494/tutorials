## Windows 11 VM Install
Create qemu VM using Virt-Viewer and the latest Windows11 ISO

Allocate at least 2 cores and 4Gb of RAM and 60GB of storage (You can get away with 50GB but its windows so ya know).

Before we start the install, we need to customize the configuration before you install Windows 11. Here is a list of the changes needed 

## XML Changes 
*(You need to enable XML editing in the Virt-Viewer settings)*

Find the `<clock offset="localtime">` . It should look like this.
```xml
  <clock offset="localtime">
    <timer name="rtc" tickpolicy="catchup"/>
    <timer name="pit" tickpolicy="delay"/>
    <timer name="hpet" present="no"/>
    <timer name="hypervclock" present="yes"/>
  </clock>
```
Change it to look like this
```xml
  <clock offset="localtime">
    <timer name="hpet" present="yes"/>
    <timer name="hypervclock" present="yes"/>
  </clock>
```

## Hardware Requirment Changes
```
Overview>Firmware>UEFI
CPUs>Topology>Manually set CPU topology>Configure for your CPU
Add Hardware>TPM>Type>Emulated
  Advanced options>Model>CRB
  Advanced options>Version>2.0
```

## Change Disks and NIC to use VirtIO and add [VirtIO ISO](https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/?C=M;O=D) 
```
Add Hardware>Storage>Device type>CDROM device
  Select VirtIO ISO
  Bus type>SATA
SATA Disk 1>Disk bus>VirtIO
NIC [MAC ADDRESS]>Device Model>virtio
```

## Install Windows 11
The Windows Install will be the same as any normal install the only difference is you need to load the VirtIO Drivers when you select the disk to install to.
		Once you reach the screen to choose a disk select "Custom".
		At first no drives will appear. Thats fine. Just select "Load Driver" and hit okay. Select the w11 driver from the list. Then you will see the drive appear. Select it and finish the installation

You will notice that the VM does not have internet. That is normal just finish the install as an offline install.

## Post Install
Once Windows is installed open the VirtIO disk with Windows Explorer and you should see a few .msi and .exe files. You want to install the virtio-win-guest-tools.exe. This will handle installing your network drivers and video drivers

##### Notes
If you are doing GPU Passthrough with an NVIDIA Card and Drivers. The NVIDIA drivers will be loaded before the QXL drivers which will cause issues with things like screen resizing